/**
 * @format
 */
import React, { Component } from 'react';
import { AppRegistry, View } from 'react-native';
import { Provider, observer } from 'mobx-react/native';

import App from './App';
import { name as appName } from './app.json';

import Store from './src/Store';

import NameComponent from './src/components/NameComponent';

const store = new Store();
@observer
class MainApp extends Component {
  render() {
    return (
      <Provider store={store}>
        <View style={{ flex: 1 }}>
          <App />
          <NameComponent />
        </View>
      </Provider>
    );
  }
}

AppRegistry.registerComponent(appName, () => MainApp);
