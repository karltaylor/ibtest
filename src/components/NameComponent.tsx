import React from 'react';
import { View, Text } from 'react-native';
import { inject, observer } from 'mobx-react/native';

interface NameComponentProps {
  store: {
    nameToUpperCase: string;
  };
}

const _NameComponent = (props: NameComponentProps): Void => (
  <View
    style={{
      position: 'absolute',
      position: 'absolute',
      top: 50,
      right: 30
    }}
  >
    <Text style={{ fontSize: 18 }}>
      {props.store.nameToUpperCase ? props.store.nameToUpperCase : 'No Name'}
    </Text>
  </View>
);

export default (NameComponent = inject('store')(observer(_NameComponent)));
