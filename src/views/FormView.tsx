import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableOpacity
} from 'react-native';
import { SafeAreaView } from 'react-navigation';

import { inject, observer } from 'mobx-react/native';

interface FormViewProps {
  store: {
    nameToUpperCase: string;
  };
  navigation: NavigationScreenProp;
}

class FormView extends Component<FormViewProps, { name: string }> {
  private static navigationOptions = {
    header: null
  };

  private state = {
    name: ''
  };

  private handleSubmit = (): void => {
    // we do some shit here.
    this.props.store.setName(this.state.name);
    this.props.navigation.push('NameView');
  };

  public render(): void {
    const buttonIsActive = this.state.name.length > 0;

    return (
      <SafeAreaView style={styles.container}>
        <Text style={{ fontSize: 18, textAlign: 'center' }}>Enter Name</Text>
        <View style={{ flexDirection: 'row', padding: 30 }}>
          <TextInput
            style={{
              flex: 1,
              borderRadius: 5,
              borderWidth: 1,
              borderColor: 'grey',
              padding: 15
            }}
            placeholder="Enter name"
            value={this.state.name}
            onChangeText={(name): void => {
              this.setState({ name });
            }}
          />
        </View>
        <View style={{ flexDirection: 'row', paddingHorizontal: 30 }}>
          <TouchableOpacity
            disabled={!buttonIsActive}
            style={{
              flex: 1,
              backgroundColor: buttonIsActive
                ? 'rgb(50, 50, 255)'
                : 'rgb(200, 200, 200)',
              borderRadius: 5,
              padding: 15
            }}
            onPress={this.handleSubmit}
          >
            <Text
              style={{
                color: buttonIsActive ? 'white' : 'rgb(125, 125, 125)',
                textAlign: 'center'
              }}
            >
              Submit
            </Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    );
  }
}

export default inject('store')(observer(FormView));

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
});
