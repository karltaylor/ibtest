import React, { Component } from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';
import { SafeAreaView } from 'react-navigation';
import { inject, observer } from 'mobx-react/native';
import { NavigationScreenProp } from 'react-navigation';

interface NameView {
  nameToUpperCase: string;
  navigation: NavigationScreenProp;
}

class NameView extends Component<NameView> {
  private static navigationOptions = {
    header: null
  };

  public render(): void {
    return (
      <SafeAreaView style={styles.container}>
        <Text style={{ fontSize: 18 }}>
          Your name is {this.props.store.nameToUpperCase}
        </Text>
        <TouchableOpacity
          style={{ marginTop: 15 }}
          onPress={(): void => this.props.navigation.goBack()}
        >
          <Text>Go Back</Text>
        </TouchableOpacity>
      </SafeAreaView>
    );
  }
}

export default inject('store')(observer(NameView));

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
});
