import { computed, observable, action } from 'mobx';

class Store {
  @observable name = '';

  @action
  setName(name) {
    this.name = name;
  }

  @computed get nameToUpperCase() {
    return this.name.toUpperCase();
  }
}
export default Store;
