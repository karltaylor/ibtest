1. Install dependencies

```bash
$ yarn install
# $ npm i
```

2. Run packager

```bash
$ yarn start
# $ npm start
```

3. In another terminal window

```bash
$ react-native run-ios
# or `open .` and open the ibTest.xcodeproj to run via Xcode.
```
