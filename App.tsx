/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/emin93/react-native-template-typescript
 *
 * @format
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';

import FormView from './src/views/FormView';
import NameView from './src/views/NameView';

const AppNavigator = createStackNavigator({
  FormView,
  NameView
});

export default createAppContainer(AppNavigator);
